import React from 'react';
import ComponenteTeste from './ComponenteTeste';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import Conta from './Conta';
import ScrollList from './ScrollList';

class App extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      ocultar: false,
      items: [1, 2, 3, 4, 5, 6]
    }
  }

  handleOcultar = () => (
    this.setState({
      ocultar: !this.state.ocultar
    })
  )

  handleAdd = () => (
    this.setState({
      items: [...this.state.items, new Date().getTime()]
    })
  )

  render() {
    return (
      <Router>
        <div className="App">
          {
            !this.state.ocultar &&
            <ComponenteTeste val1={10} />
          }

          <button onClick={this.handleOcultar}>Ocultar/Fechar</button> <br />

          <ScrollList items={this.state.items} /> <br />
          <button onClick={this.handleAdd}>Adicionar</button> <br />

          <Link to='/conta/1'>Conta 1</Link> <br />
          <Link to='/conta/2'>Conta 2</Link> <br />
          <Link to='/conta/3'>Conta 3</Link> <br />

          <Route path='/conta/:num' component={Conta} />
        </div>
      </Router>
    )
  }
}

export default App;
