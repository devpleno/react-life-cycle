import React from 'react';

class ComponenteTeste extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            count: 0,
            val1: 1
        }

        console.log('constructor')
    }

    // static getDerivedStateFromProps(props, state) {
    //     console.log('getDerivedStateFromProps', props, state)

    //     return null
    //     // return { val1: props.val1 * 2 }
    // }

    render() {
        console.log('render')

        return (
            <div>
                {JSON.stringify(this.state)}
                <p>Meu componente: {this.state.count}</p>
            </div>
        )
    }

    componentDidMount() {
        console.log('componentDidMount')

        this.intervalo = setInterval(() => {
            this.setState({
                count: this.state.count + 1
            })
        }, 60 * 1000)
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log('shouldComponentUpdate', nextProps, nextState)

        return nextState.count % 2 === 0
    }

    componentDidUpdate(prevProps) {
        console.log('componentDidUpdate', prevProps)

        // if (this.props.val1 !== prevProps.val1) {
        //     console.log('mudou')
        // }
    }

    componentWillUnmount() {
        console.log('componentWillUnmount')

        clearInterval(this.intervalo)

        this.setState({
            count: 0
        })
    }

}

export default ComponenteTeste