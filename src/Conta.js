import React, { Component } from 'react';
import axios from 'axios'

class Conta extends Component {

    async loadData(id) {
        const data = await axios.get('http://httpbin.org/ip')
        console.log('loadData', data.data.origin, id)
    }

    componentDidMount() {
        console.log('mount', this.props)
 
        this.loadData(this.props.match.params.num)
    }

    componentDidUpdate(prevProps) {
        console.log('update', prevProps)

        if(this.props.match.params.num !== prevProps.match.params.num) {
            this.loadData(this.props.match.params.num)
        }
    }

    render() {
        return (
            <div>
                <h1>Conta</h1>

                <h3>{this.props.match.params.num}</h3>

                <pre>
                    {JSON.stringify(this.props, null, 4)}
                </pre>

            </div>
        )
    }
}

export default Conta