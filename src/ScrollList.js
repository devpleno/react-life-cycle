import React from 'react';

class ScrollList extends React.Component {

    constructor(props) {
        super(props)
        this.listRef = React.createRef()
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        console.log('getSnapshotBeforeUpdate', this.listRef)

        if(prevProps.items.length < this.props.items.length) {
            const list = this.listRef.current
           
            // console.log(list.scrollTop, list.scrollHeight)
            return list.scrollHeight
        }

        return null
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log('componentDidUpdate')

        if(snapshot) {
            const list = this.listRef.current
            list.scrollTop = snapshot
        }
    }

    render() {
        return (
            <div>
                <ul className='list' ref={this.listRef}>
                    {this.props.items.map((obj, i) => (
                        <li key={i}>{obj}</li>
                    ))}
                </ul>
            </div>
        )
    }
}

export default ScrollList